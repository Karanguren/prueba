import Vue from 'vue';
import Router from 'vue-router';

import Inicio from '@/components/Inicio'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/inicio',
      name: 'Inicio',
      component: Inicio,
      props: { page: 1 },
      alias: '/'
    },
    
  ]
})